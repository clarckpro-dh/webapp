resource "aws_key_pair" "key" {
  key_name   = var.key_name
  public_key = file("./keypair.pub")
}

resource "aws_instance" "my_ec2" {
  ami           = var.ami_id
  instance_type = "t2.micro"
  key_name = aws_key_pair.key.key_name
  security_groups = [aws_security_group.allow_ssh.name, aws_security_group.allow_http.name]
  tags = var.tags
}

output "arn" {
  value = aws_instance.my_ec2.arn
}

output "public_ip" {
  value = aws_instance.my_ec2.public_ip
}
