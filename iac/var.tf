variable "ami_id" {
  description = "ubuntu ami id"
  default = "ami-04505e74c0741db8d"
}

variable "tags" {
  type = map(string)
  default = {
    "Name"        = "grupo-0-c4-webapp"
    "Terraform"   = "true"
    "Environment" = "dev"
  }
}

variable "key_name" {
  default = "grupo-0-c4-keypair"
}