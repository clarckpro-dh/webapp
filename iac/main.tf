
# ==================================================================
# Declaramos el Cloud Provider con el que queremos trabajar
terraform {
  # Le decimos que queremos:
  # a. la versión del binario de terraform mayor o igual a 0.12
  required_version = ">=0.12"
  required_providers {
    aws = {
      # Especificamos desde donde queremos descargar el binario:
      source = "hashicorp/aws" 
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}
